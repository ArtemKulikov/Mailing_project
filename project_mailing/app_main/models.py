from django.db import models
from app_mailing.models import Mailing
from app_client.models import Client
from django.utils import timezone

class Message(models.Model):

	date_time = models.DateTimeField(auto_now_add=True, verbose_name='дата и время сообщения')
	status = models.IntegerField(db_index=True, verbose_name='статус отправки')
	id_mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='id рассылки')
	id_client = models.ForeignKey(Client, on_delete=models.CASCADE, verbose_name='id клиента')


class File(models.Model):
	file = models.FileField(upload_to='clients/')
	created_at = models.DateField(auto_now_add=True)
