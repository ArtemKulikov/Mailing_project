from django.urls import path, include
from .views import MainView, start_mailings, get_statistic, get_statistic_detail

urlpatterns = [
	path('', MainView.as_view(), name='main_page'),
	path('start_mailings/', start_mailings, name='mailings'),
	path('get_statistic/', get_statistic, name='statistic'),
	path('get_statistic/<int:pk>', get_statistic_detail, name='statistic_detail'),
]