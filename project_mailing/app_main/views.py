import datetime
import json
import os.path
import time
import requests
import logging
from django.shortcuts import render, redirect
from django.views import View
from django.db.models import Q, Count
from django.utils.timezone import make_aware
from .forms import FileForm
from .models import Message, File
from app_mailing.models import Mailing
from app_client.models import Client
from django.conf import settings

logger = logging.getLogger(__name__)


class MainView(View):

	@staticmethod
	def get(request):
		form_file_clients = FileForm()
		return render(request, template_name='index.html', context={'form_file_clients': form_file_clients})

	@staticmethod
	def post(request):
		form_file_clients = FileForm(request.POST, request.FILES)
		if form_file_clients.is_valid():
			file = form_file_clients.cleaned_data.get('file')
			file_object = File(file=file)
			file_object.save()
			try:
				with open(os.path.join(settings.BASE_DIR, 'media/') + 'clients/' + str(file) ,encoding='utf-8') as f:
					clients = json.load(f)
					for client in clients:
						Client.objects.create(phone_number=client['phone_number'], mobile_operator_code=client['mobile_operator_code'],
											  tag=client['tag'], time_zone=client['time_zone'])
			except:
				logger.warning('неудачный импорт клиентов')
		return render(request, template_name='index.html', context={'form_file_clients': form_file_clients})


def start_mailings(request):
	logger.info('Старт сервиса рассылок')
	while True:

		mailings = Mailing.objects.filter(date_time__gt=datetime.datetime.now().astimezone()).filter(
			it_sent=False).order_by(
			'date_time')
		if mailings and (datetime.datetime.now().astimezone() - mailings[0].date_time).seconds < 0.1:
			logger.info(f'Старт рассылки {mailings[0].id}')
			f1 = mailings[0].filters.filter(title='код').values('value')
			f2 = mailings[0].filters.filter(title='тег').values('value')
			if f1 or f2:
				clients = Client.objects.filter(Q(mobile_operator_code__in=f1) | Q(tag__in=f2))
			else:
				clients = Client.objects.all()

			for i, client in enumerate(clients):
				send_message(i, client, mailings[0])
			mailings[0].it_sent = True
			mailings[0].save()


def send_message(id_msg, client, mailing):
	url = f"https://probe.fbrq.cloud/v1/send/{id_msg}"
	payload = {"id": id_msg, "phone": client.phone_number, "text": mailing.text}
	headers = {
		'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9'
						 '.eyJleHAiOjE2OTA1MzY2OTQsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6Imt1bGlrb3ZfYXYifQ'
						 '.-9ecxUcLLzl8jb4ZsbsdeNI-DgoiuVixc_v6h7vLa7E',
		'Content-Type': 'text/plain'
	}
	try:
		response = requests.request("POST", url, headers=headers, json=payload, timeout=2)
		logger.info(f'Отправка сообщения клиенту {client.phone_number}')
	except:
		logger.warning(f'Ошибка отправки сообщения клиенту {client.phone_number}')
	Message.objects.create(status=response.status_code, id_mailing=mailing, id_client=client)


def get_statistic(request):
	mailings = Mailing.objects.filter(date_time__lt=datetime.datetime.now().astimezone()).filter(
		it_sent=True).annotate(num_ok_messages=Count('message', filter=Q(message__status__startswith=2)),
							   num_fale_messages=Count('message', filter=Q(message__status__startswith=5)))

	return render(request, template_name='statistic_page.html', context={'mailings': mailings})


def get_statistic_detail(request, pk):
	mailing = Mailing.objects.annotate(num_ok_messages=Count('message', filter=Q(message__status__startswith=2)),
									   num_fale_messages=Count('message', filter=Q(message__status__startswith=5))).get(
		id=pk)

	return render(request, template_name='statistic_detail_page.html', context={'mailing': mailing})
