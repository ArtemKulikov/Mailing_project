from django.urls import path, include
from .api import Mailings
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'api/mailings', Mailings, basename='api_mailings')

urlpatterns = [] + router.urls
