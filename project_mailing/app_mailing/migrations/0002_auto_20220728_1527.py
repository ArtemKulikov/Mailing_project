# Generated by Django 3.2.13 on 2022-07-28 12:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_mailing', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='filter',
            name='value',
            field=models.CharField(default=911, max_length=20, verbose_name='значение'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='filter',
            name='title',
            field=models.CharField(max_length=20, verbose_name='код мобильного оператора, тег'),
        ),
    ]
