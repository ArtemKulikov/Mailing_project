# Generated by Django 3.2.13 on 2022-07-28 12:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app_mailing', '0003_auto_20220728_1541'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mailing',
            name='filters',
        ),
        migrations.AddField(
            model_name='mailing',
            name='filters',
            field=models.ManyToManyField(to='app_mailing.Filter'),
        ),
    ]
