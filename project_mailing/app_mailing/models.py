from django.db import models


class Filter(models.Model):
    """ Модель фильтр"""
    title = models.CharField(max_length=20, verbose_name='код мобильного оператора, тег')
    value = models.CharField(max_length=20, verbose_name='значение')

    def __str__(self):
        return f'{self.title} {self.value}'


class Mailing(models.Model):
    """Модель Рассылка"""
    date_time = models.DateTimeField(db_index=True, verbose_name='дата и время запуска рассылки')
    text = models.CharField(max_length=300, verbose_name='текст сообщения для доставки клиенту')
    filters = models.ManyToManyField(Filter)
    date_time_off = models.DateTimeField(verbose_name='дата и время окончания рассылки')
    published = models.BooleanField()
    it_sent = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.text}'
