from django.contrib import admin
from .models import Filter, Mailing


class FilterInline(admin.TabularInline):
    model = Mailing.filters.through


@admin.register(Filter)
class FilterAdmin(admin.ModelAdmin):
    pass


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    inlines = [
        FilterInline,
    ]
    exclude = ('filters',)
