from rest_framework.relations import PrimaryKeyRelatedField

from .models import Filter, Mailing
from rest_framework import serializers


class FilterSerializer(PrimaryKeyRelatedField, serializers.ModelSerializer):
    class Meta:
        model = Filter
        fields = ['value']


class MailingSerializer(serializers.ModelSerializer):
    """Сериализатор модели Рассылка"""
    filters = FilterSerializer(queryset=Filter.objects.all(), many=True)

    class Meta:
        model = Mailing
        fields = ['id', 'date_time', 'text', 'filters', 'date_time_off']
