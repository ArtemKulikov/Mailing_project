from .models import Client
from rest_framework import serializers


class ClientSerializer(serializers.ModelSerializer):
	"""Сериализатор модели Клиент"""

	class Meta:
		model = Client
		fields = ['id', 'phone_number', 'mobile_operator_code', 'tag', 'time_zone']
