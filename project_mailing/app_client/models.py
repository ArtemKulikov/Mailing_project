from django.db import models


class Client(models.Model):
    """Модель Клиент"""
    phone_number = models.IntegerField(db_index=True, verbose_name='номер телефона клиента в формате 7XXXXXXXXXX (X - цифра от 0 до 9)')
    mobile_operator_code = models.IntegerField(verbose_name='код мобильного оператора')
    tag = models.CharField(max_length=50, verbose_name='тег (произвольная метка)')
    time_zone = models.IntegerField(verbose_name='часовой пояс')
