from django.contrib import admin
from .models import Client


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['phone_number', 'tag', 'time_zone']
    list_filter = ['tag', 'time_zone']
    search_fields = ['phone_number', 'tag']
