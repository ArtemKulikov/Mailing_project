from django.urls import path, include
from .api import Clients
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'api/clients', Clients, basename='api_clients')

urlpatterns = [] + router.urls
